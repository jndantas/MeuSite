<h3>Bem vindo ao painel de gerenciamento</h3>

<p>O que você quer fazer agora?</p>

<p>
<a href="/admin/pages" class="btn btn-primary btn-lg">
    <span style="font-size:70px;"><i class="far fa-file-alt"></i></span><br>
        Gerenciar Páginas
</a>
<a href="admin/users" class="btn btn-success btn-lg">
    <span style="font-size:70px;"><i class="far fa-user"></i></span><br>
        Gerenciar Usuários
</a>
</p>

<a href="/admin/pages">Administrar Páginas</a>